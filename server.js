var app  = require('express')(),
	http = require('http').Server(app),
	io   = require('socket.io')(http);

app.get('/', function(req, res){
	res.sendfile(__dirname+'/index.html');
	console.log("Se a cargado el index");
});

app.get('/presentacion', function(req, res){
	res.sendfile(__dirname+'/javascript/index.html');
	console.log("Se a cargado el index");
});

io.on('connection', function(socket){
	socket.on('arriba', function(msg){
  		io.emit("arriba", msg);
  	});

	socket.on('abajo', function(msg){
  		io.emit("abajo", msg);
  	});

  	socket.on('derecha', function(msg){
  		io.emit("derecha", msg);
  	});

  	socket.on('izquierda', function(msg){
  		io.emit("izquierda", msg);
  	});
});

http.listen(80, function(){
	console.log("Servidor escucha en el puerto 80");
});
